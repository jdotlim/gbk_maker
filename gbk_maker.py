import argparse
from pathlib import Path

from BCBio import GFF
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Data import CodonTable

def main():
    parser = argparse.ArgumentParser(description="Create a GenBank flatfile")
    parser.add_argument(
        "sequence",
        metavar="SEQUENCE",
        type=Path,
        help="FASTA file with a single sequence to annotate"
    )
    parser.add_argument(
        "annotations",
        metavar="ANNOTATIONS",
        type=Path,
        help="TSV file of feature coordinates and annotations"
    )
    parser.add_argument(
        "output",
        metavar="GENBANK",
        type=Path,
        help="Output GenBank flat file"
    )
    parser.add_argument(
        "--translation-table",
        dest="transl_table",
        type=int,
        default=11,
        choices=list(CodonTable.unambiguous_dna_by_id.keys())
    )

    args = parser.parse_args()

    with open(args.sequence, "rt") as fna, open(args.annotations, "rt") as gff:
        seq_dict = SeqIO.to_dict(SeqIO.parse(fna, "fasta"))
        for rec in GFF.parse(gff, base_dict=seq_dict):
            rec.annotations["molecule_type"] = "DNA"
            rec.description = "{0[0]}:{0[1]}-{0[2]} locus".format(
                rec.annotations["sequence-region"][0]
            )
            rec.annotations["accession"] = ""
            rec.annotations["source"] = "Unknown."
            rec.annotations["organism"] = "Unknown."
            rec.annotations["topology"] = "linear"

            for feat in rec.features:
                if "ID" in feat.qualifiers:
                    del feat.qualifiers["ID"]
                if "phase" in feat.qualifiers:
                    del feat.qualifiers["phase"]
                if feat.type == "CDS":
                    feat.qualifiers["codon_start"] = 1
                    feat.qualifiers["transl_table"] = args.transl_table
                    feat.qualifiers["translation"] = feat.extract(
                        rec.seq
                    ).translate(table=args.transl_table)
                
            SeqIO.write(rec, args.output, "genbank")

if __name__ == "__main__":
    main()
